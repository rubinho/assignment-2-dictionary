ASM_FLAGS=-f elf64

.PHONY: clean
clean:
	rm -f *.o

%.o: %.asm
	nasm $(ASM_FLAGS) $< -o $@

dict.o: dict.asm lib.inc colon.inc

main.o: main.asm lib.inc dict.inc words.inc


program: main.o lib.o dict.o
	ld -o $@ $^
.PHONY: test
test: program
	python3 test.py
